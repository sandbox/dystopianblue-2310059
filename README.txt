BULLHORN MODULE FOR DRUPAL 7
============================

INTRODUCTION
------------
The Bullhorn module connects Drupal to the Bullhorn applicant tracking system for recruiters. Currently, it uses the Bullhorn REST API to import jobs into Drupal and integrates with the Views module to display job listings on a site. Other data, such as job submissions and candidate info, could be potentially added in future releases to further expand the module's capabilities. For more information on Bullhorn, please visit http://www.bullhorn.com/.

This module and its developer is not in any way affiliated, associated with or endorsed by Bullhorn Inc.


REQUIREMENTS
------------  
 * A Bullhorn account with a valid username and password that will be used primarily for the Bullhorn REST API.
 * A Client ID and Client Secret to access the Bullhorn REST API. Contact Bullhorn Support if you need to obtain this info.


INSTALLATION
------------
    1. Download and unpack the Bullhorn module to the "sites/all/modules" directory.
    2. Enable the module on the "Modules" admin page.
    3. Grant Bullhorn administration access to users on the "People" > "Permissions" admin page.


CONFIGURATION
-------------
Enter your Client ID, Client Secret, Bullhorn Username and Bullhorn Password on the "Configuration" > "Web services" > "Bullhorn" admin page.  If your supplied credentials are invalid, an error message will appear: "Unable to login to the Bullhorn REST API with your settings. Please verify and try again."


JOB ORDER
---------
To import jobs from Bullhorn and configure the import settings, go to the "Configuration" > "Web services" > "Bullhorn" > "Job Order" tab.  Choose the type of jobs to import into Drupal.  There is also the option to synchronize jobs with Bullhorn based on the site's cron schedule.  The maximum number of jobs to import can be chosen as well.  Bullhorn restricts the number of job results returned to 500 per API call, so setting it considerably higher will require more time for processing.  If an error occurs indicating that a job failed to import, please copy the error message and notify the module developer.


DRUSH
-----
To import jobs from Bullhorn using Drush, use the command "drush bullhorn-import-jobs".


RESOURCES
---------
 * Getting Started with the Bullhorn REST API - http://developer.bullhorn.com/articles/getting_started
 * REST API 1.1 Reference - http://developer.bullhorn.com/sites/default/files/BullhornRESTAPI_0.pdf


MAINTAINERS
-----------
 * Ray Yick (dystopianblue) - https://www.drupal.org/u/dystopianblue
