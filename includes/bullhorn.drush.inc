<?php
/**
 * @file
 * Drush integration with the Bullhorn module.
 */

/**
 * Implements hook_drush_command().
 */
function bullhorn_drush_command() {
  return array(
    'bullhorn-import-jobs' => array(
      'description' => dt('Imports jobs from Bullhorn.'),
      'aliases' => array('bij'),
      'examples' => array(
        'drush bullhorn-import-jobs' => 'Imports jobs from Bullhorn.',
      ),
    ),
  );
}

/**
 * Runs function to import jobs from Bullhorn.
 */
function drush_bullhorn_import_jobs() {
  module_load_include('inc', 'bullhorn', 'includes/bullhorn.admin');
  bullhorn_import_job_order();
}
