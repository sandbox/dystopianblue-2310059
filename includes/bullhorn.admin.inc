<?php
/**
 * @file
 * Functionality and helper functions for Bullhorn administration.
 */

/**
 * Form builder function for general Bullhorn configuration settings.
 */
function bullhorn_admin_settings() {
  $form['bullhorn_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('bullhorn_client_id', ''),
    '#maxlength' => 40,
    '#description' => t('The unique OAuth ID string given to you for the Bullhorn REST API.'),
    '#required' => TRUE,
  );
  $form['bullhorn_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('bullhorn_client_secret', ''),
    '#maxlength' => 40,
    '#description' => t('The secret OAuth key given to you for the Bullhorn REST API.'),
    '#required' => TRUE,
  );
  $form['bullhorn_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullhorn Username'),
    '#default_value' => variable_get('bullhorn_username', ''),
    '#maxlength' => 40,
    '#description' => t('Bullhorn client username.'),
    '#required' => TRUE,
  );
  $form['bullhorn_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullhorn Password'),
    '#default_value' => variable_get('bullhorn_password', ''),
    '#maxlength' => 40,
    '#description' => t('Bullhorn client password.'),
    '#required' => TRUE,
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'bullhorn_test_settings';
  return $form;
}

/**
 * Form builder function for job import settings.
 */
function bullhorn_admin_job_order() {
  $form['bullhorn_job_order'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import settings'),
  );

  $form['bullhorn_job_order']['bullhorn_job_order_isopen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Import open jobs only'),
    '#default_value' => variable_get('bullhorn_job_order_isopen', 1),
  );

  $form['bullhorn_job_order']['bullhorn_job_order_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically sync jobs with Bullhorn'),
    '#default_value' => variable_get('bullhorn_job_order_cron', 1),
    '#description' => t('Jobs are updated periodically based on <a href="@cron">cron</a>.', array('@cron' => url('admin/config/system/cron'))),
  );

  $form['bullhorn_job_order']['bullhorn_job_order_status'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Status'),
    '#options' => drupal_map_assoc(array(
      'Accepting Candidates',
      'Currently Interviewing',
      'Covered',
      'Offer Out',
      'Placed',
    )),
    '#default_value' => variable_get('bullhorn_job_order_status', array('Accepting Candidates', 'Currently Interviewing')),
    '#description' => t('Jobs with the above selected status will be imported only.'),
  );

  $form['bullhorn_job_order']['bullhorn_job_order_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of jobs to import'),
    '#default_value' => variable_get('bullhorn_job_order_count', BULLHORN_MAX_SEARCH_COUNT),
    '#maxlength' => 4,
    '#size' => 5,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form = system_settings_form($form);
  $form['actions']['submit']['#value'] = 'Import';
  $form['#submit'][] = 'bullhorn_import_job_order';
  return $form;
}

/**
 * Imports jobs from Bullhorn into the Drupal database.
 */
function bullhorn_import_job_order() {
  if (empty($_SESSION['bhRestToken']) || empty($_SESSION['restUrl']) || empty($_SESSION['bhRestTokenExpiresIn']) || time() >= $_SESSION['bhRestTokenExpiresIn']) {
    bullhorn_rest_login();
  }

  // Builds query string based on job import settings.
  $query['isDeleted'] = 'isDeleted:0';

  if (variable_get('bullhorn_job_order_isopen') === 1) {
    $query['isOpen'] = 'isOpen:1';
  }

  if (variable_get('bullhorn_job_order_status', FALSE)) {
    foreach (array_filter(variable_get('bullhorn_job_order_status')) as $status) {
      $query_status[] = 'status:"' . $status . '"';
    }
    $query['status'] = '(' . implode(' OR ', $query_status) . ')';
  }

  $query_string = str_replace(' ', '%20', implode(' AND ', $query));

  $success = $fail = 0;
  $count = variable_get('bullhorn_job_order_count') > BULLHORN_MAX_SEARCH_COUNT ? BULLHORN_MAX_SEARCH_COUNT : variable_get('bullhorn_job_order_count');

  // Iterate through set of search calls if the maximum number of jobs to import is greater than BULLHORN_MAX_SEARCH_COUNT.
  for ($i = 0; $i < ceil(variable_get('bullhorn_job_order_count') / BULLHORN_MAX_SEARCH_COUNT); $i++) {
    // Generate REST API call.
    $ch = curl_init($_SESSION['restUrl'] . 'search/JobOrder?fields=*&query=' . $query_string . '&count=' . $count . '&start=' . $i * BULLHORN_MAX_SEARCH_COUNT);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('BhRestToken: ' . $_SESSION['bhRestToken']));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $rs = curl_exec($ch);
    curl_close($ch);
    $json_rs = json_decode($rs);

    if (!empty($json_rs->data)) {
      foreach ($json_rs->data as $job_rs) {
        $job = array();
        foreach ($job_rs as $k => $v) {
          // Exclude _score field.
          if ($k != '_score') {
            if (is_object($v)) {
              foreach ($v as $sk => $sv) {
                // Exclude data fields and ensure field exists in database.
                if ($sk != 'data' && db_field_exists('bullhorn_job_order', $k . ucfirst($sk))) {
                  if (stristr($k . ucfirst($sk), 'date')) {
                    // Convert Bullhorn timestamp in milliseconds to seconds.
                    $job[$k . ucfirst($sk)] = (int) ($sv / 1000);
                  }
                  else {
                    $job[$k . ucfirst($sk)] = !is_bool($sv) ? $sv : (int) $sv;
                  }
                }
              }
            }
            elseif (db_field_exists('bullhorn_job_order', $k)) {
              if (stristr($k, 'date')) {
                // Convert Bullhorn timestamp in milliseconds to seconds.
                $job[$k] = (int) ($v / 1000);
              }
              else {
                $job[$k] = !is_bool($v) ? $v : (int) $v;
              }
            }
          }
        }

        try {
          // Update jobs in database.
          $jid = db_merge('bullhorn_job_order')
          ->key(array('id' => $job['id']))
          ->fields($job)
          ->execute();

          $success++;
        }
        catch(Exception $e) {
          drupal_set_message(t('A job failed to import. Message = %message. Please report this error to the module developer.', array('%message' => $e->getMessage())), 'error');
          $fail++;
        }
      }
    }
  }

  if ($success > 0) {
    drupal_set_message(t('@success jobs were successfully imported from Bullhorn.', array('@success' => $success)), 'status');
  }
  if ($fail > 0) {
    drupal_set_message(t('@fail jobs failed to import from Bullhorn.', array('@fail' => $fail)), 'status');
  }
}

/**
 * Gets authorization code.
 */
function bullhorn_get_auth_code() {
  $ch = curl_init(BULLHORN_OAUTH_ENDPOINT . 'authorize');

  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
  curl_setopt($ch, CURLOPT_HEADER, TRUE);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, 'client_id=' . variable_get('bullhorn_client_id', '') . '&response_type=code&redirect_uri=' . BULLHORN_REDIRECTURI . '&state=' . BULLHORN_STATE . '&action=Login&username=' . variable_get('bullhorn_username', '') . '&password=' . variable_get('bullhorn_password', ''));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  $rs = curl_exec($ch);
  curl_close($ch);

  if (preg_match('/code=([^&]+)/', $rs, $r)) {
    return urldecode($r[1]);
  }
  else {
    return;
  }
}

/**
 * Get access token.
 */
function bullhorn_get_access_token() {
  // Return valid access token.
  if (!empty($_SESSION['accessToken']) && time() < $_SESSION['tokenExpiresIn']) {
    return $_SESSION['accessToken'];
  }

  // Access token expired: retrieve new access token using refresh token.
  elseif (!empty($_SESSION['accessToken']) && !empty($_SESSION['refreshToken']) && time() >= $_SESSION['tokenExpiresIn']) {
    return $_SESSION['accessToken'] = bullhorn_refresh_access_token();
  }

  // Retrieve new access token.
  else {
    $_SESSION['authCode'] = bullhorn_get_auth_code();
    return $_SESSION['accessToken'] = bullhorn_new_access_token();
  }
}

/**
 * Retrieves a new access token.
 */
function bullhorn_new_access_token() {
  $ch = curl_init(BULLHORN_OAUTH_ENDPOINT . 'token');

  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=authorization_code&code=' . $_SESSION['authCode'] . '&client_id=' . variable_get('bullhorn_client_id', '') . '&client_secret=' . variable_get('bullhorn_client_secret', '') . "&redirect_uri=" . BULLHORN_REDIRECTURI);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  $rs = curl_exec($ch);
  curl_close($ch);
  $json_rs = json_decode($rs);

  if (isset($json_rs->access_token)) {
    $_SESSION['tokenExpiresIn'] = time() + (int) $json_rs->expires_in;
    $_SESSION['refreshToken'] = $json_rs->refresh_token;
    return $json_rs->access_token;
  }
  else {
    $_SESSION['tokenExpiresIn'] = '';
    $_SESSION['refreshToken'] = '';
    return '';
  }
}

/**
 * Retrieves a new access token using the refresh token.
 */
function bullhorn_refresh_access_token() {
  $ch = curl_init(BULLHORN_OAUTH_ENDPOINT . 'token');

  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=refresh_token&refresh_token=' . $_SESSION['refreshToken'] . '&client_id=' . variable_get('bullhorn_client_id', '') . '&client_secret=' . variable_get('bullhorn_client_secret', ''));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  $rs = curl_exec($ch);
  curl_close($ch);
  $json_rs = json_decode($rs);

  if (isset($json_rs->access_token)) {
    $_SESSION['tokenExpiresIn'] = time() + (int) $json_rs->expires_in;
    $_SESSION['refreshToken'] = $json_rs->refresh_token;
    return $json_rs->access_token;
  }
  else {
    $_SESSION['tokenExpiresIn'] = '';
    $_SESSION['refreshToken'] = '';
    return '';
  }
}

/**
 * Logs into the Bullhorn REST API and retrieves a REST token and URL.
 */
function bullhorn_rest_login() {
  $at = bullhorn_get_access_token();

  $ch = curl_init(BULLHORN_REST_ENDPOINT . 'login?version=*&ttl=' . BULLHORN_REST_TTL . '&access_token=' . $at);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  $rs = curl_exec($ch);
  curl_close($ch);
  $json_rs = json_decode($rs);

  if (isset($json_rs->BhRestToken)) {
    $_SESSION['bhRestToken'] = $json_rs->BhRestToken;
    $_SESSION['restUrl'] = $json_rs->restUrl;
    $_SESSION['bhRestTokenExpiresIn'] = time() + (BULLHORN_REST_TTL * 60);
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Tests Bullhorn credentials.
 */
function bullhorn_test_settings() {
  $auth_code = bullhorn_get_auth_code();

  if (!$auth_code) {
    drupal_set_message(t('Unable to login to the Bullhorn REST API with your settings. Please verify and try again.'), 'error');
  }
}
